import sys, getopt
from neuro_lib.inputs.devices import OpenBCI
from neuro_lib.outputs.file_writers import file_writer

opts = {}

# list of BCI kits number and MAC addresses
kit_mac_addresses = { 
    2 : 'e0:06:15:36:28:44'
}
# If set to none it will connect to the first one found
kit_mac_address = None

# BCI board object
board = None

def print_help():
    message = """
        Usage: main.py --kit <kit_number>'
        Example: main.py --kit 3'
        Options:
            -h  --help      Print this help message
            -k  --kit       Select the Ganglion kit to use
        """
    print(message)
    sys.exit()
    
def connect_to_board(kit_mac_address=None):
    print("Target MAC address: " + str(kit_mac_address))
    global board
    board = OpenBCI.OpenBCIGanglionExt(mac=kit_mac_address)
    print('Connected! Board id: ' + board.board_id)
    print("Yuuh hu!")
    return board

def main(argv):
    
    # force args
    global opts
    if 1 <= len (argv):
        try:
            opts, args = getopt.getopt(argv,"hk:",["--help", "kit="])
        except getopt.GetoptError:
            print('main.py -kit <kitnumber>')
            sys.exit(2) 
    # read args
    global kit_mac_address
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print_help()
        elif opt in ('-k', '--kit'):
            print('Connecting to **MindLAB** kit #' + arg + ' ...')
            kit_number = int(arg)
            kit_mac_address = kit_mac_addresses[kit_number]
    
    # Connect and pass sample to writer
    global board
    board = connect_to_board(kit_mac_address)
    
    board.start_stream(callback=file_writer.file_writer.write_file_raw)

if __name__ == "__main__":
    main(sys.argv[1:])