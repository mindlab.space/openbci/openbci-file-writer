import sys
import atexit
import numpy as np
from bluepy.btle import Scanner
from pyOpenBCI import ganglion


SAMPLE_RATE = 200.0  # Hz
MCP3912_Vref = 1.2
MCP3912_gain = 1.0
scale_fac_uVolts_per_count = (MCP3912_Vref * 1000000) / (8388607.0 * MCP3912_gain * 1.5 * 51.0)
scale_fac_volts_per_count = (MCP3912_Vref * 1000000) / (8388607.0 * MCP3912_gain * 1.5 * 51.0)


class OpenBCIGanglionExt(ganglion.OpenBCIGanglion):
    """ Extend original OpenBCI Class """
    
    def __init__(self, mac=None, max_packets_skipped=15):
        self.board_id = None
        if not mac:
            self.mac_address = self.find_mac()
        else:
            self.mac_address = mac
            self.find_id()
        self.max_packets_skipped = max_packets_skipped
        self.streaming = False
        self.board_type = 'Ganglion'
        atexit.register(self.disconnect)
        self.connect()
        
    def find_mac(self):
        """Finds and returns the mac address of the first Ganglion board found"""
        #print('Looking for mac address...')
        scanner = Scanner()
        devices = scanner.scan(5)

        if len(devices) < 1:
            raise OSError('No nearby Devices found. Make sure your Bluetooth Connection is on.')

        else:
            gang_macs = []
            for dev in devices:
                for adtype, desc, value in dev.getScanData():
                    if desc == 'Complete Local Name' and value.startswith('Ganglion'):
                        gang_macs.append(dev.addr)
                        #print(value)
                        self.board_id = value

        if len(gang_macs) < 1:
            raise OSError('Cannot find OpenBCI Ganglion Mac address.')
        else:
            print("Found Ganglion with mac address: "+ gang_macs[0])
            return gang_macs[0]
        
    def find_id(self):
        scanner = Scanner()
        devices = scanner.scan(5)
        for dev in devices:
            for adtype, desc, value in dev.getScanData():
                if desc == 'Complete Local Name' and value.startswith('Ganglion'):
                    if dev.addr == self.mac_address:
                        self.board_id = value
                        
    def start_stream(self, callback):
        """Start handling streaming data from the Ganglion board. Call a provided callback for every single sample that is processed."""
        if not self.streaming:
            self.streaming = True
            self.dropped_packets = 0
            self.write_command('b')

        if not isinstance(callback, list):
            callback = [callback]

        while self.streaming:
            try:
                self.ganglion.waitForNotifications(1./SAMPLE_RATE)
            except Exception as e:
                print(e)
                print('Something went wrong')
                sys.exit(1)

            samples = self.ble_delegate.getSamples()
            if samples:
                for sample in samples:
                    sample.channels_data = sample.channels_data * scale_fac_uVolts_per_count
                    for call in callback:
                        call(sample=sample)