# OpenBCI file writer

A simple code sample that show how to start reading brain-data 
and write them to a text file using OpenBCI Ganglion board 
and pyOpenBCI library.   

Brain-data, in volts units, will be written in ```testfile.txt```.   
Example, excerpt of the file (no biosensor was connect to the board):
```
[ 4.67861456 12.16402386  4.98715628 11.40295426]
[-19.30910228 -27.57241073 -35.72913203 -37.26810077]
[17.09508165 19.01552016 11.82743288 12.19207311]
[41.90557643 56.32849972 58.22836878 53.01307861]
[10.21366615  9.21324297  4.91609819  3.14899557]
[-19.84016805 -25.16391531 -34.10227565 -35.00172153]
[14.12186136 19.8457779  18.70510848 15.61782126]
```

## Pre-requisite (How-to install)
How-to setup the environment: [how-to](docs/how-to.md)   

## Usage
```
$ python main.py
```