# This project how-to

## System
Ubuntu 18.04

## Setup

```
# Conda
conda update conda
conda create --name OBCI_file_writer
conda activate OBCI_file_writer
conda install python=3.8.0

# Python libs
pip install numpy pyserial bitstring xmltodict requests bluepy
pip install pyOpenBCI

# enable bluepy-helper functionalities 
sudo setcap 'cap_net_raw,cap_net_admin+eip' ~/anaconda3/envs/OBCI_file_writer/lib/python3.8/site-packages/bluepy/bluepy-helper

```

## useful commands
List Bluetooth MAC addresses:   
```
sudo hcitool -i hci0 lescan
```